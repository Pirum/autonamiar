imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }

# Put parameters here that don't need to change on each machine where the app is deployed
# http://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration
parameters:
    locale: pl
    app.path.post_images: /uploads/images/post

framework:
    #esi:             ~
    translator:      { fallbacks: ["%locale%"] }
    secret:          "%secret%"
    router:
        resource: "%kernel.root_dir%/config/routing.yml"
        strict_requirements: ~
    form:            ~
    csrf_protection: ~
    validation:      { enable_annotations: true }
    #serializer:      { enable_annotations: true }
    templating:
        engines: ['twig']
    default_locale:  "%locale%"
    trusted_hosts:   ~
    trusted_proxies: ~
    session:
        # http://symfony.com/doc/current/reference/configuration/framework.html#handler-id
        handler_id:  session.handler.native_file
        save_path:   "%kernel.root_dir%/../var/sessions/%kernel.environment%"
    fragments:       ~
    http_method_override: true
    assets: ~

# Twig Configuration
twig:
    debug:            "%kernel.debug%"
    strict_variables: "%kernel.debug%"
    form_themes:

        # Default:
        # - form_div_layout.html.twig

        # Bootstrap:
        - bootstrap_3_layout.html.twig
        # - bootstrap_3_horizontal_layout.html.twig

        # Foundation
        # - foundation_5_layout.html.twig

# Doctrine Configuration
doctrine:
    dbal:
        driver:   pdo_mysql
        host:     "%database_host%"
        port:     "%database_port%"
        dbname:   "%database_name%"
        user:     "%database_user%"
        password: "%database_password%"
        charset:  UTF8
        # if using pdo_sqlite as your database driver:
        #   1. add the path in parameters.yml
        #     e.g. database_path: "%kernel.root_dir%/data/data.db3"
        #   2. Uncomment database_path in parameters.yml.dist
        #   3. Uncomment next line:
        #     path:     "%database_path%"

    orm:
        auto_generate_proxy_classes: "%kernel.debug%"
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true

# Swiftmailer Configuration
swiftmailer:
    transport: "%mailer_transport%"
    host:      "%mailer_host%"
    username:  "%mailer_user%"
    password:  "%mailer_password%"
    spool:     { type: memory }

# KNP Paginator Configuration
knp_paginator:
    page_range: 5                      # default page range used in pagination control
    default_options:
        page_name: page                # page query parameter name
        sort_field_name: sort          # sort field query parameter name
        sort_direction_name: direction # sort direction query parameter name
        distinct: true                 # ensure distinct results, useful when ORM queries are using GROUP BY statements
    template:
        pagination: KnpPaginatorBundle:Pagination:twitter_bootstrap_v3_pagination.html.twig     # sliding pagination controls template
        sortable: KnpPaginatorBundle:Pagination:sortable_link.html.twig # sort link template

# FOS User Configuration
fos_user:
    db_driver: orm # other valid values are 'mongodb', 'couchdb' and 'propel'
    firewall_name: main
    user_class: AppBundle\Entity\User
    use_username_form_type: false
    #registration:
    #    form:
    #        name: app_user_registration
    service:
        mailer: fos_user.mailer.twig_swift
    from_email:
        address: sebek11388@wp.pl
        sender_name: Seba Test
    registration:
        confirmation:
            enabled: true
            from_email:
                address: sebek11388@wp.pl
                sender_name: Noreply Seba
            template: '@FOSUser/Registration/email.txt.twig'
    resetting:
        email:
            #from_email:
                #address: resetting@localhost
                #sender_name: Reset Seba
            template: email/password_resetting.email.twig

# EasyAdmin Configuration
easy_admin:
    design:
        menu:
            - { label: 'Strona publiczna', route: 'homepage', target: '_blank' }
            - { label: 'Katalog firm' }
            - { entity: 'Company', params: { sortField: 'id' } }
            - { entity: 'Service', params: { sortField: 'parent', sortDirection: 'ASC' } }
            - { entity: 'Province', params: { sortField: 'province', sortDirection: 'ASC' } }
            - { entity: 'VehicleBrand', params: { sortField: 'brand', sortDirection: 'ASC' } }
            - { entity: 'Rating', params: { sortField: 'createdAt', sortDirection: 'DESC' } }
            - { label: 'Blog' }
            - { entity: 'Post', params: { sortField: 'id', sortDirection: 'ASC' } }
            - { entity: 'Comment', params: { sortField: 'createdAt' } }
            - { label: 'Administracja' }
            - { entity: 'User', params: { sortField: 'username', sortDirection: 'ASC' } }
    formats:
        date:     'd/m/Y'
        time:     'H:i'
        datetime: 'd/m/Y H:i:s'
    list:
        actions:
            - { name: 'show', icon: 'show' }
            - { name: 'edit', icon: 'pencil' }
            - { name: 'delete', icon: 'remove' }
    entities:
        Post:
            class: AppBundle\Entity\Post
            label: 'Artykuły'
            list:
                title: "Artykuły"
                fields:
                    - id
                    - { property: 'image', type: 'image', base_path: %app.path.post_images% }
                    - title
                    - created
                    - updatedAt
            show:
                fields:
                    - { property: 'image', type: 'image', base_path: %app.path.post_images% }
                    - title
                    - lead
                    - content
                    - created
            form:
                fields:
                    - title
                    - lead
                    - content
                    - created
                    - { property: 'imageFile', type: 'vich_image' }
        Comment:
            class: AppBundle\Entity\Comment
            label: 'Komentarze'
            list:
                title: "Komentarze"
            form:
                fields: ['content']
        User:
            class: AppBundle\Entity\User
            label: 'Użytkownicy'
            list:
                title: "Użytkownicy"
                fields:
                    - id
                    - username
                    - email
                    - enabled
                    - locked
                    - roles
                    - lastLogin
                    
            edit:
                fields:
                    - { property: 'username', type: 'text', type_options: { disabled: true } }
                    - email
                    - enabled
                    - locked
                    - expired
                    - { property: 'roles', type: 'choice', type_options: { multiple: true, choices: { 'Zwykły użytkownik': 'ROLE_USER', 'Administrator': 'ROLE_ADMIN' } } }
                    - { property: 'plainPassword', type: 'text', type_options: { required: false} }
                form_options: { validation_groups: ['Profile'] }
            
            new:
                fields:
                    - username
                    - email
                    - enabled
                    - { property: 'plainPassword', type: 'text', type_options: { required: true } }
                    - { property: 'roles', type: 'choice', type_options: { multiple: true, choices: { 'Zwykły użytkownik': 'ROLE_USER', 'Administrator': 'ROLE_ADMIN' } } }
                form_options: { validation_groups: ['Registration'] }
        Service:
            class: AppBundle\Entity\Service
            label: 'Usługi'
            list:
                title: "Usługi w katalogu usług"
                fields:
                    - { property: 'serviceName', label: 'Nazwa usługi' }
                    - { property: 'parent', label: 'Usługa nadrzędna' }
            edit:
                fields:
                    - { property: 'serviceName', label: 'Nazwa usługi' }
                    - { property: 'parent', label: 'Usługa nadrzędna' }
            new:
                fields:
                    - { property: 'serviceName', label: 'Nazwa usługi' }
                    - { property: 'parent', label: 'Usługa nadrzędna' }
                    
        Company:
            class: AppBundle\Entity\Company
            label: 'Katalog firm'
            list:
                title: "Katalog firm"
                fields:
                    - id
                    - { property: 'name', label: 'Nazwa firmy' }
                    - { property: 'published', label: 'Widoczne' }
                    - { property: 'createdAt', label: 'Data utworzenia' }
                    - { property: 'modifiedAt', label: 'Data modyfikacji' }
            edit:
                fields:
                    - name
                    - address
                    - city
                    - postcode
                    - province
                    - telephone
                    - fax
                    - mobile
                    - email
                    - webpage
                    - published
                    - misc
                    - user
                    - services
            
            new:
                fields:
                    - name
                    - address
                    - city
                    - postcode
                    - province
                    - telephone
                    - fax
                    - mobile
                    - email
                    - webpage
                    - published
                    - misc
                    - user
                    - services
        Province:
            class: AppBundle\Entity\Province
            label: 'Kraje i województwa'
            list:
                title: "Lista województw"
                fields:
                    - id
                    - { property: 'province', label: 'Województwo' }
                    - { property: 'country', label: 'Kraj' }
        VehicleBrand:
            class: AppBundle\Entity\VehicleBrand
            label: 'Marki pojazdów'
            list:
                title: "Marki pojazdów"
                fields:
                    - { property: 'brand', label: 'Marka' }
        
        Rating:
            class: AppBundle\Entity\Rating
            label: 'Rating usług'
            list:
                title: "Oddane głosy na usługi firm"
                fields:
                    - { property: 'createdAt', label: 'Dodano' }
                    - { property: 'comment', label: 'Komentarz' }
                    - { property: 'userName', label: 'Użytkownik' }
                    - { property: 'ip', label: 'Adres IP' }
                    - { property: 'service', label: 'Usługa' }
                    - { property: 'company', label: 'Firma' }
                    - { property: 'vote', label: 'Głos' }
            new:
                fields:
                    - { property: 'comment', label: 'Komentarz' }
                    - { property: 'userName', label: 'Nazwa użytkownika' }
                    - { property: 'service', label: 'Usługa' }
                    - { property: 'company', label: 'Firma' }
                    - { property: 'vote', label: 'Głos' }
            edit:
                fields:
                    - { property: 'comment', label: 'Komentarz' }
                    - { property: 'userName', label: 'Nazwa użytkownika' }
                    - { property: 'service', label: 'Usługa' }
                    - { property: 'company', label: 'Firma' }
                    - { property: 'vote', label: 'Głos' }

# VICH Uploader Configuration (file uploader)
vich_uploader:
    db_driver: orm
    mappings:
        post_images:
            uri_prefix:         %app.path.post_images%
            upload_destination: %kernel.root_dir%/../web/uploads/images/post
            namer:              vich_uploader.namer_origname
            inject_on_load:     false
            delete_on_update:   true
            delete_on_remove:   true
            