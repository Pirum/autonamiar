<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\Company;
use AppBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class UserController extends Controller
{
    /**
     * ###@Route("/register", name="newCompany")
     */
    public function newAction(Request $request)
    {
    	$user = new User();
    	$company = new Company();
        $user->getCompany()->add($company);
    	
    	$form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // ... maybe do some form processing, like saving the User and Company objects
            
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $user = $form->getData();
            // $company = $form->getData();
    
            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', "Company dodane pomyślnie");
            //return $this->redirectToRoute('task_success');
        }
        else {
            $this->addFlash('error', "Wystąpił błąd");
        }
        
        return $this->render('AppBundle:User:new.html.twig', array(
            'form' => $form->createView()
        ));
    }

}
