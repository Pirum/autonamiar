<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\Comment;
use AppBundle\Entity\Company;
use AppBundle\Form\CommentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $qb = $this->getDoctrine()
            ->getManager()
            ->createQueryBuilder()
            ->from('AppBundle:Post', 'p')
            ->select('p');

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $qb,
            $request->query->get('page', 1),
            20
        );
        
        return $this->render('default/index.html.twig', array(
            'posts' => $pagination
        ));
    }
    
    /**
     * @Route("/article/{id}", name="post_show")
     */
    public function showAction(Post $post, Request $request)
    {
    	$form = null;
    	
        // jesli user jest zalogowany
        if ($user = $this->getUser()) {
        	$comment = new Comment();
        	$comment->setPost($post);
	        $comment->setUser($user);
	        
	        $form = $this->createForm(CommentType::class, $comment);
		    $form->handleRequest($request);
		    
		    if ($form->isValid()) {
			    $em = $this->getDoctrine()->getManager();
			    $em->persist($comment);
			    $em->flush();
			    $this->addFlash('success', "Komentarz zostal pomyslnie dodany");
			    
			    return $this->redirectToRoute('post_show', array('id' => $post->getId()));
		    }
	    }
	    
	    return $this->render("default/show.html.twig", array(
	    	'post' => $post,
	    	'form' => is_null($form) ? $form : $form->createView()
	    ));
    }
    
    /**
     * @Route("/catalog", name="catalog_list")
     */
    public function companyListAction(Request $request)
    {
        $qb = $this->getDoctrine()
            ->getManager()
            ->createQueryBuilder()
            ->from('AppBundle:Company', 'c')
            ->select('c')
            ->where('c.published = 1')
            ->orderBy('c.id', 'DESC');

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $qb,
            $request->query->get('page', 1),
            20
        );
        
        return $this->render('default/catalog/list.html.twig', array(
            'catalog' => $pagination
        ));
    }
    
    /**
     * @Route("/catalog/{id}.html", name="card_show")
     */
    public function cardShowAction(Company $company, Request $request)
    {
	    return $this->render("default/catalog/show.html.twig", array(
	    	'card' => $company
	    ));
    }
}
