<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Company;
use AppBundle\Form\CompanyType;
use Symfony\Component\HttpFoundation\Request;

class CompanyController extends Controller
{
    /**
     * @Route("/register-company", name="newCompany")
     */
    public function newAction(Request $request)
    {
    	$form = null;
    	
        // jesli user jest zalogowany
        if ($user = $this->getUser()) {
        	$company = new Company();
	        $company->setUser($user);
	        $company->setCreatedBy($user);
	        $company->setModifiedBy($user);
	        // by default, every new Company is automatically published
	        // @TODO: send confirmation email to the user who is adding new Company
	        $company->setPublished(true);
	        
	        $form = $this->createForm(CompanyType::class, $company);
		    $form->handleRequest($request);
		    
		    if ($form->isValid()) {
			    $em = $this->getDoctrine()->getManager();
			    $em->persist($company);
			    $em->flush();
			    $this->addFlash('success', "Firma zostala pomyslnie dodana");
			    
			    return $this->redirectToRoute('card_show', array('id' => $company->getId()));
		    }
	    }
	    else {
    	    $this->addFlash('error', "Wystąpił błąd");
    	    $this->redirectToRoute('catalog_list');
	    }
	    
	    return $this->render("AppBundle:Company:new.html.twig", array(
	    	'form' => $form->createView()
	    ));
    }
    
    /**
     * @Route("/my-cards", name="myCompanyList")
     */
    public function myCompanyListAction(Request $request)
    {
        $user = $this->getUser();
        $qb = $this->getDoctrine()
            ->getManager()
            ->createQueryBuilder()
            ->from('AppBundle:Company', 'c')
            ->select('c')
            ->where('c.published = 1')
            ->andWhere('c.user = ' . $user->getId())
            ->orderBy('c.id', 'DESC');

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $qb,
            $request->query->get('page', 1),
            20
        );
        
        return $this->render('default/catalog/user_list.html.twig', array(
            'catalog' => $pagination
        ));
    }
}
