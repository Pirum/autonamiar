<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
//use Symfony\Component\Form\Extension\Core\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CompanyType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('name', TextType::class, array(
        	'label' => 'Nazwa firmy'
        	))
        ->add('address', TextType::class, array(
        	'label' => 'Adres'
        	))
        ->add('city', TextType::class, array(
        	'label' => 'Miasto'
        	))
        ->add('province', EntityType::class, array(
            'class' => 'AppBundle:Province',
            'choice_label' => 'province',
            'placeholder' => 'Wybierz województwo z listy',
        	))
        ->add('postcode', TextType::class, array(
        	'label' => 'Kod pocztowy'
        	))
        ->add('telephone', NumberType::class, array(
        	'label' => 'Telefon stacjonarny'
        	))
        ->add('fax', NumberType::class, array(
        	'label' => 'Faks'
        	))
        ->add('mobile', NumberType::class, array(
        	'label' => 'Telefon komórkowy'
        	))
        ->add('email', EmailType::class, array(
        	'label' => 'Email'
        	))	
        ->add('webpage', UrlType::class, array(
        	'label' => 'Adres www'
        	))
        ->add('misc', TextareaType::class, array(
        	'label' => 'Dodatkowe informacje'
        	))
        ->add('services', EntityType::class, array(
                'class'    => 'AppBundle\Entity\Service',
                'label'    => 'Usługi',                
                'multiple' => true,
                'expanded' => true,
                'group_by' => 'serviceName'
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Company'
        ));
    }

}
