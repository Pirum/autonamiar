<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Service
 *
 * @ORM\Table(name="service")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ServiceRepository")
 */
class Service
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="serviceName", type="string", length=255)
     */
    private $serviceName;
    
    /**
     * Many Services have Many Companys.
     * @ORM\ManyToMany(targetEntity="Company", mappedBy="services")
     */
    private $companys;
    
    /**
     * @var Service
     *
     * @ORM\OneToMany(targetEntity="Service", mappedBy="parent")
     */
    private $childrens;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToOne(targetEntity="Service", inversedBy="childrens")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;
    
    
    public function __construct() 
    {
        $this->companys = new ArrayCollection();
        $this->childrens = new ArrayCollection();
    }
    
    public function __toString() 
    {
        return (string) $this->serviceName;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set serviceName
     *
     * @param string $serviceName
     *
     * @return Service
     */
    public function setServiceName($serviceName)
    {
        $this->serviceName = $serviceName;

        return $this;
    }

    /**
     * Get serviceName
     *
     * @return string
     */
    public function getServiceName()
    {
        return $this->serviceName;
    }

    /**
     * Add company
     *
     * @param \AppBundle\Entity\Company $company
     *
     * @return Service
     */
    public function addCompany(\AppBundle\Entity\Company $company)
    {
        $this->companys[] = $company;

        return $this;
    }

    /**
     * Remove company
     *
     * @param \AppBundle\Entity\Company $company
     */
    public function removeCompany(\AppBundle\Entity\Company $company)
    {
        $this->companys->removeElement($company);
    }

    /**
     * Get companys
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompanys()
    {
        return $this->companys;
    }

    /**
     * Add child
     *
     * @param \AppBundle\Entity\Service $child
     *
     * @return Service
     */
    public function addChild(\AppBundle\Entity\Service $child)
    {
        $this->childrens[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \AppBundle\Entity\Service $child
     */
    public function removeChild(\AppBundle\Entity\Service $child)
    {
        $this->childrens->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->childrens;
    }

    /**
     * Set parent
     *
     * @param \AppBundle\Entity\Service $parent
     *
     * @return Service
     */
    public function setParent(\AppBundle\Entity\Service $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \AppBundle\Entity\Service
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add children
     *
     * @param \AppBundle\Entity\Service $children
     *
     * @return Service
     */
    public function addChildren(\AppBundle\Entity\Service $children)
    {
        $this->childrens[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \AppBundle\Entity\Service $children
     */
    public function removeChildren(\AppBundle\Entity\Service $children)
    {
        $this->childrens->removeElement($children);
    }

    /**
     * Get childrens
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildrens()
    {
        return $this->childrens;
    }
}
